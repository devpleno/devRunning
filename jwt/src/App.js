import React, { Component } from 'react';

import Admin from './screen/admin'
import Restrito from './screen/restrito'
import Home from './screen/home'
import Login from './screen/login'
import CreateAccount from './screen/CreateAccount'

import store from './redux'
import { Provider } from 'react-redux'

import { Route, BrowserRouter as Router } from 'react-router-dom'

import { Container } from 'semantic-ui-react'

import Header from './Header'
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Container>
            <Header />
            
            <Route component={Home} path='/' exact />
            <Route component={Admin} path='/admin' />
            <Route component={Restrito} path='/restrito' />
            <Route component={CreateAccount} path='/create-account' />
            <Route component={Login} path='/login' />
          </Container>
        </Router>
      </Provider>
    );
  }
}

export default App;
