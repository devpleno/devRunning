import { takeLatest, all, put } from 'redux-saga/effects'
import { Types } from '../actionCreators'

import { getUsers, getUser, removeUser, updateUser } from './users'
import { getRuns, createRun, removeRun } from './runs'
import { login, destroyAuth, auth, updateProfile, createProfile } from './auth'

import ActionCreators from '../actionCreators'
import Api from '../../service/api'

export default function* rootSaga() {

    const devURL = 'http://localhost:3001'
    const prodURL = 'https://www.alvoideal.com.br/ws'

    const baseURL = process.env.NODE_ENV === 'development' ? devURL : prodURL
    const api = new Api(baseURL)

    yield all([
        takeLatest(Types.SIGN_IN_REQUEST, login({ api })),

        takeLatest(Types.AUTH_REQUEST, auth({ api })),
        takeLatest(Types.DESTROY_AUTH_REQUEST, destroyAuth),

        takeLatest(Types.GET_RUNS_REQUEST, getRuns({ api })),
        takeLatest(Types.CREATE_RUN_REQUEST, createRun({ api })),
        takeLatest(Types.REMOVE_RUN_REQUEST, removeRun({ api })),

        takeLatest(Types.UPDATE_PROFILE_REQUEST, updateProfile({ api })),
        takeLatest(Types.CREATE_PROFILE_REQUEST, createProfile({ api })),

        takeLatest(Types.GET_USERS_REQUEST, getUsers({ api })),
        takeLatest(Types.GET_USER_REQUEST, getUser({ api })),
        takeLatest(Types.REMOVE_USER_REQUEST, removeUser({ api })),
        takeLatest(Types.UPDATE_USER_REQUEST, updateUser({ api })),

        put(ActionCreators.authRequest())
    ])
}