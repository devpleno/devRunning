import ActionCreators from '../actionCreators'
import { put, call } from 'redux-saga/effects'

export const getRuns = ({ api }) => function* (action) {
    let filtro = (action.admin) ? "?admin=true" : ""

    const runs = yield call(api.getRuns, filtro)
    yield put(ActionCreators.getRunsSuccess(runs.data.data))
}

export const createRun = ({api}) => function* (action) {
    const newRun = yield call(api.createRun, action.run)

    if (newRun && newRun.data && newRun.data.error) {
        yield put(ActionCreators.createRunFailure(newRun.data.message))
    } else {
        yield put(ActionCreators.createRunSuccess(newRun.data))
    }
}

export const removeRun = ({ api }) => function* (action) {
    const runDelete = yield call(api.removeRun, action.id);

    if (runDelete && runDelete.data && runDelete.data.error) {
        yield put(ActionCreators.removeRunFailure(runDelete.data.message))
    } else {
        yield put(ActionCreators.removeRunSuccess(action.id))
    }
}