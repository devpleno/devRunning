import ActionCreators from '../actionCreators'
import { put, call } from 'redux-saga/effects'

import jwtDecoded from 'jwt-decode'

export const login = ({api}) => function* (action) {

    let userLogin = {
        email: action.email,
        passwd: action.passwd
    }

    const login = yield call(api.login, userLogin)

    if (login.data.token) {
        let token = login.data.token
        localStorage.setItem('token', token)

        const user = jwtDecoded(token)
        localStorage.setItem('user', JSON.stringify(user))

        yield put(ActionCreators.signInSuccess(user))
    } else {
        localStorage.removeItem('token')
        yield put(ActionCreators.signInFailure(login.data.message))
    }
}

export const auth = ({api}) => function* () {
    const token = localStorage.getItem('token')

    if (token) {

        try {
            const user = yield call(api.getUser, 'me')
            yield put(ActionCreators.authSuccess(user.data))
        } catch (e) {
            yield put(ActionCreators.authFailure('invalid token'))
        }

    } else {
        yield put(ActionCreators.authFailure('no token'))
    }
}

export const updateProfile = ({api}) => function* (action) {
    yield call(api.updateUser, action.user)
    yield put(ActionCreators.updateProfileSuccess(action.user))
}

export const createProfile = ({api}) => function* (action) {
    const userToSave = {
        name: action.user.name,
        email: action.user.email,
        unit: action.user.unit,
        timezone: action.user.timezone,
        passwd: action.user.passwd
    }

    const newUser = yield call(api.createUser, userToSave)

    if(newUser && newUser.data && newUser.data.error) {
        yield put(ActionCreators.createProfileFailure(newUser.data.message))
    } else {
        yield put(ActionCreators.createProfileSuccess(userToSave))
        
        // faz login automatico
        yield put(ActionCreators.signInRequest(userToSave.email, userToSave.passwd))
    }

}

export function* destroyAuth() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')

    yield put(ActionCreators.destroyAuthSuccess())
}