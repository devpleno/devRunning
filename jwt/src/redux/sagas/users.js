import ActionCreators from '../actionCreators'
import { put, call } from 'redux-saga/effects'

export const getUser = ({ api }) => function* (action) {
    const user = yield call(api.getUser, action.id);
    yield put(ActionCreators.getUserSuccess(user.data))
}

export const getUsers = ({ api }) => function* () {
    const users = yield call(api.getUsers);
    yield put(ActionCreators.getUsersSuccess(users.data))
}

export const removeUser = ({ api }) => function* (action) {
    const userDelete = yield call(api.removeUser, action.id);

    if (userDelete && userDelete.data && userDelete.data.error) {
        yield put(ActionCreators.removeUserFailure(userDelete.data.message))
    } else {
        yield put(ActionCreators.removeUserSuccess(action.id))
    }
}

export const updateUser = ({ api }) => function* (action) {
    yield call(api.updateUser, action.user);
    yield put(ActionCreators.updateUserSuccess(action.user))
}