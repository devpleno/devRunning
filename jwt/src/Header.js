import React from 'react'
import { connect } from 'react-redux'
import ActionCreators from './redux/actionCreators'

import { Link } from 'react-router-dom'
import { Menu, Dropdown, Image } from 'semantic-ui-react'

const Header = props => {
    return (
        <Menu>
            <Menu.Item as={Link} to='/'>
                <Image src={'/logo.png'} size='small' />
            </Menu.Item>

            {props.auth.isAuth && props.auth.user.role === 'admin' &&
                <Menu.Item as={Link} to='/admin'>Administrador</Menu.Item>
            }

            {props.auth.isAuth &&
                <Menu.Item as={Link} to='/restrito'>Restrito</Menu.Item>
            }

            {!props.auth.isAuth &&
                <Menu.Item as={Link} to='/create-account'>Criar uma conta</Menu.Item>
            }

            {!props.auth.isAuth &&
                <Menu.Item as={Link} to='/login'>Entrar</Menu.Item>
            }

            {props.auth.isAuth &&
                <Menu.Menu position='right'>
                    <Dropdown item text={props.auth.user.name}>
                        <Dropdown.Menu>
                            <Dropdown.Item as={Link} to='/restrito/my-account'>Minha conta</Dropdown.Item>
                            <Dropdown.Item as={Link} to='/restrito/change-pass'>Alterar senha</Dropdown.Item>
                            <Dropdown.Item onClick={() => props.logout()}>Sair</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Menu.Menu>
            }
        </Menu>
    )
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        signIn: (email, passwd) => dispatch(ActionCreators.signInRequest(email, passwd)),
        logout: () => dispatch(ActionCreators.destroyAuthRequest())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)