import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

import Header from './elements/Header'
import Users from './Users'
import EditUser from './EditUser'

const Home = () => <div>Tela Home admin</div>
const CreateUser = () => <div>Tela Home CreateUser</div>

const Admin = (props) => {

    if(!props.auth.isAuth) {
        return <Redirect to='/login' />
    }

    if(props.auth.user.role !== 'admin') {
        return <Redirect to='/restrito' />
    }

    return (
        <div>
            <Header />

            <div>
                <Route component={Home} path={props.match.path + '/'} exact />
                <Route component={EditUser} path={props.match.path + '/users/:id/edit'} exact />

                <Route component={CreateUser} path={props.match.path + '/create-user'} />
                <Route component={Users} path={props.match.path + '/users'} exact />
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps)(Admin)