import React, { Component } from 'react'
import ActionCreators from '../../redux/actionCreators'
import { connect } from 'react-redux'

import { Button, Segment, Form } from 'semantic-ui-react'

class EditUser extends Component {
    state = {
        name: '',
        email: '',
        role: '',
    }

    componentDidMount() {
        this.props.reset()
        this.props.load(this.props.match.params.id)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.users && nextProps.users.user && nextProps.users.user.name) {
            this.setState({
                ...nextProps.users.user
            });
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSave = () => {
        this.setState({ error: '' })

        this.props.save({
            name: this.state.name,
            email: this.state.email,
            role: this.state.role,
            id: this.props.match.params.id
        })

        setTimeout(() => {
            this.props.reset()
            this.props.history.push("/admin/users")
        }, 3000)
    }

    render() {
        return (
            <div>
                <h1>Editar usuario</h1>

                {this.props.users.saved &&
                    <Segment color='green'>Salvo com sucesso</Segment>
                }

                {this.props.users.error &&
                    <Segment color='red'>{this.props.users.errorMessage}</Segment>
                }

                {!this.props.users.saved &&
                    <Form>
                        <Form.Field>
                            <label>Nome</label>
                            <input type='text' value={this.state.name} name='name' onChange={this.handleChange} />
                        </Form.Field>

                        <Form.Field>
                            <label>E-mail</label>
                            <input type='text' value={this.state.email} name='email' onChange={this.handleChange} />
                        </Form.Field>


                        <Form.Field>
                            <label>Permissão</label>
                            <select value={this.state.role} name='role' onChange={this.handleChange}>
                                <option value='admin'>Administrador</option>
                                <option value='user'>Restrito</option>
                            </select>
                        </Form.Field>

                        <Form.Field>
                            <Button onClick={() => this.handleSave()}>Salvar</Button>
                        </Form.Field>
                    </Form>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        users: state.users
    }
}

const mapDispatchToProps = dispatch => {
    return {
        save: (user) => dispatch(ActionCreators.updateUserRequest(user)),
        reset: () => dispatch(ActionCreators.updateUserReset()),
        load: (id) => dispatch(ActionCreators.getUserRequest(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditUser);