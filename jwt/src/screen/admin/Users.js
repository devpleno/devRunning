import React, { Component } from 'react';
import ActionCreators from '../../redux/actionCreators'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { Table, Button } from 'semantic-ui-react'

class Users extends Component {

    componentDidMount() {
        this.props.load()
    }

    renderUser = (user, i) => {
        return (
            <Table.Row key={i}>
                <Table.Cell>
                    {user.name}
                </Table.Cell>
                <Table.Cell>
                    {user.passwd}
                </Table.Cell>
                <Table.Cell>
                    {user.email}
                </Table.Cell>
                <Table.Cell>
                    {user.role}
                </Table.Cell>
                <Table.Cell>
                    {user.unit}
                </Table.Cell>
                <Table.Cell>
                    {user.timezone}
                </Table.Cell>
                <Table.Cell>
                    <Button basic color='blue' as={Link} to={`/admin/users/${user.id}/edit`}>Editar</Button>
                    <Button basic color='red' onClick={() => this.props.remove(user.id)}>Excluir</Button>
                </Table.Cell>
            </Table.Row>
        )
    }

    render() {
        return (
            <div>
                <h1>Usuários</h1>
                <Button as={Link} to='/admin/create-user'>Novo usuário</Button>

                {this.props.users.isLoading &&
                    <div>Carregando...</div>
                }

                {!this.props.users.isLoading && this.props.users.data.length === 0 &&
                    <div>Nenhum registro</div>
                }

                {!this.props.users.isLoading && this.props.users.data.length > 0 &&
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Nome</Table.HeaderCell>
                                <Table.HeaderCell>Password</Table.HeaderCell>
                                <Table.HeaderCell>Email</Table.HeaderCell>
                                <Table.HeaderCell>Permissão</Table.HeaderCell>
                                <Table.HeaderCell>Unit</Table.HeaderCell>
                                <Table.HeaderCell>Timezone</Table.HeaderCell>
                                <Table.HeaderCell>Ações</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {this.props.users.data.map((obj, i) => this.renderUser(obj, i))}
                        </Table.Body>
                    </Table>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        users: state.users,
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        load: () => dispatch(ActionCreators.getUsersRequest()),
        remove: (id) => dispatch(ActionCreators.removeUserRequest(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Users)