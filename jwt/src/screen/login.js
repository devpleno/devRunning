import React, { Component } from 'react'
import ActionCreator from '../redux/actionCreators'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

import { Form, Button } from 'semantic-ui-react'

class Login extends Component {

    state = {
        email: 'tulio@devpleno.com',
        passwd: 'abc123'
    }

    handleValidar = () => {
        const { email, passwd } = this.state

        this.props.login(email, passwd)
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {

        if (this.props.auth.isAuth) {

            if (this.props.auth.user.role === 'admin') {
                return <Redirect to='/admin' />
            } else {
                return <Redirect to='/restrito' />
            }

        }

        return (
            <div>
                <h1>Login</h1>

                <Form>
                    <Form.Field>
                        <label>E-mail</label>
                        <input type="text" name='email' value={this.state.email} onChange={this.handleChange} />
                    </Form.Field>

                    <Form.Field>
                        <label>Password</label>
                        <input type="password" name='passwd' value={this.state.passwd} onChange={this.handleChange} />
                    </Form.Field>

                    <Button onClick={() => this.handleValidar()}>Validar</Button>

                    {this.props.auth.error && this.props.auth.errorMessage &&
                        <div>
                            Erro encontrado: {this.props.auth.errorMessage}
                        </div>
                    }
                </Form>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = dispach => {
    return {
        login: (email, passwd) => dispach(ActionCreator.signInRequest(email, passwd))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login)