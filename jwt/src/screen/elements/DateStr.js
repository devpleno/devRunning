import React from 'react'
import moment from 'moment-timezone'

const DateStr = ({date, timezone}) => {

    const dataEntrada = moment.tz(date, 'GMT')
    const dataSaida = dataEntrada.clone().tz(timezone)

    return (
        <div>
            {dataSaida.format('DD/MM/YYYY HH:mm')}
        </div>
    )
}

export default DateStr