import React from 'react'

import { Image } from 'semantic-ui-react'

const Home = () => {
    return (
        <div>
            <h1>Tela Home</h1>
            <Image src={'/logo-home.png'} size='medium' />
        </div>
    )
}

export default Home