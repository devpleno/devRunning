import React, { Component } from 'react'
import ActionCreators from '../redux/actionCreators'
import { connect } from 'react-redux'
import moment from 'moment-timezone'

import { Redirect } from 'react-router-dom'
import { Button, Segment, Form } from 'semantic-ui-react'

class CreateAccount extends Component {
    state = {
        name: '',
        email: '',
        unit: 'metric',
        timezone: 'America/Sao_Paulo',
        passwd: '',
        passwd2: '',
        error: ''
    }

    componentDidMount() {
        this.props.reset()
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSave = () => {
        if (this.state.passwd !== this.state.passwd2) {

            this.setState({
                error: 'equal'
            })

        } else if (this.state.passwd.length < 6) {

            this.setState({
                error: 'length'
            })

        } else {

            this.setState({ error: '' })

            this.props.save({
                name: this.state.name,
                email: this.state.email,
                unit: this.state.unit,
                timezone: this.state.timezone,
                passwd: this.state.passwd
            })

            setTimeout(() => {
                this.props.reset()
            }, 3000)

        }
    }

    render() {

        if (this.props.auth.isAuth) {
            return <Redirect to='/restrito' />
        }

        return (
            <div>
                <h1>Criar uma conta</h1>

                {this.state.error === 'equal' &&
                    <Segment color='warning'>As senhas digitadas não conferem.</Segment>
                }

                {this.state.error === 'length' &&
                    <Segment color='warning'>Sua senha é fraca, deve possuir mais que 6 caracteres.</Segment>
                }

                {this.props.auth.saved &&
                    <Segment color='green'>Salvo com sucesso</Segment>
                }

                {this.props.auth.error &&
                    <Segment color='red'>{this.props.auth.errorMessage}</Segment>
                }

                {!this.props.auth.saved &&
                    <Form>
                        <Form.Field>
                            <label>Nome</label>
                            <input type='text' value={this.state.name} name='name' onChange={this.handleChange} />
                        </Form.Field>

                        <Form.Field>
                            <label>E-mail</label>
                            <input type='email' value={this.state.email} name='email' onChange={this.handleChange} />
                        </Form.Field>

                        <Form.Field>
                            <label>Unit</label>
                            <select value={this.state.unit} name='unit' onChange={this.handleChange}>
                                <option value='metric'>Métrico (km)</option>
                                <option value='imperial'>Imperial (mi)</option>
                            </select>
                        </Form.Field>

                        <Form.Field>
                            <label>Timezone</label>
                            <select value={this.state.timezone} name='timezone' onChange={this.handleChange}>
                                {
                                    moment.tz.names().map(tz => {
                                        return <option key={tz} value={tz}>{tz}</option>
                                    })
                                }
                            </select>
                        </Form.Field>

                        <Form.Field>
                            <label>Nova senha</label>
                            <input type='password' value={this.state.passwd} name='passwd' onChange={this.handleChange} />
                        </Form.Field>

                        <Form.Field>
                            <label>Repete nova senha</label>
                            <input type='password' value={this.state.passwd2} name='passwd2' onChange={this.handleChange} />
                        </Form.Field>

                        <Button onClick={() => this.handleSave()}>Salvar</Button>
                    </Form>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        save: (user) => dispatch(ActionCreators.createProfileRequest(user)),
        reset: () => dispatch(ActionCreators.createProfileReset())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateAccount);