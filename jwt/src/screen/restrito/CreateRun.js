import React, { Component } from 'react'
import ActionCreators from '../../redux/actionCreators'
import { connect } from 'react-redux'

import { Button, Segment, Form } from 'semantic-ui-react'

import InputMoment from 'input-moment'
import 'input-moment/dist/input-moment.css'

import moment from 'moment'
import momentTz from 'moment-timezone'

class CreateRun extends Component {
    state = {
        friendly_name: '',
        duration: '',
        distance: '',
        created: moment()
    }

    componentDidMount() {
        this.props.reset()
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSave = () => {
        this.setState({ error: '' })

        const unit = this.props.auth.user.unit
        const distance = this.state.distance

        const dataEntrada = momentTz.tz(this.state.created, this.props.auth.user.timezone)
        const dataSaida = dataEntrada.clone().utc().format('YYYY-MM-DD H:mm:ss')

        this.props.create({
            friendly_name: this.state.friendly_name,
            duration: this.state.duration,
            distance: unit === 'metric' ? distance : (distance * 1.634),
            created: dataSaida
        })

        setTimeout(() => {
            this.props.reset()
            this.props.history.push("/restrito/runs")
        }, 3000)
    }

    render() {
        return (
            <div>
                <h1>Nova corrida</h1>

                {this.props.runs.saved &&
                    <Segment color='green'>Salvo com sucesso</Segment>
                }

                {this.props.runs.error &&
                    <Segment color='red'>{this.props.runs.errorMessage}</Segment>
                }

                {!this.props.runs.saved &&
                    <Form>
                        <Form.Field>
                            <label>Friendly Name</label>
                            <input type='text' value={this.state.friendly_name} name='friendly_name' onChange={this.handleChange} />
                        </Form.Field>

                        <Form.Field>
                            <label>Duração em segundos</label>
                            <input type='text' value={this.state.duration} name='duration' onChange={this.handleChange} />
                        </Form.Field>

                        <Form.Field>
                            <label>Distância ({this.props.auth.user.unit === 'metric' ? 'km' : 'mi'})</label>
                            <input type='text' value={this.state.distance} name='distance' onChange={this.handleChange} />
                        </Form.Field>

                        <Form.Field>
                            <label>Created</label>
                            <input type='text' value={this.state.created.format('DD/MM/YYYY H:mm')} name='created' onChange={this.handleChange} />
                        </Form.Field>

                        <Form.Field>
                            <InputMoment moment={this.state.created} onChange={(val) => this.setState({ created: val })} />
                        </Form.Field>

                        <Form.Field>
                            <Button onClick={() => this.handleSave()}>Salvar</Button>
                        </Form.Field>
                    </Form>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
        runs: state.runs
    }
}

const mapDispatchToProps = dispatch => {
    return {
        create: (run) => dispatch(ActionCreators.createRunRequest(run)),
        reset: () => dispatch(ActionCreators.createRunReset())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateRun);