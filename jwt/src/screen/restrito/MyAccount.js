import React, { Component } from 'react'
import ActionCreators from '../../redux/actionCreators'
import { connect } from 'react-redux'

import { Button, Form, Segment } from 'semantic-ui-react'

import moment from 'moment-timezone'

class MyAccount extends Component {
    state = {
        unit: '',
        timezone: ''
    }

    componentDidMount() {
        this.setState({
            unit: this.props.auth.user.unit,
            timezone: this.props.auth.user.timezone
        })

        this.props.reset()
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSave = () => {
        this.props.save({
            unit: this.state.unit,
            timezone: this.state.timezone,
            id: this.props.auth.user.id
        })

        setTimeout(() => {
            this.props.reset()
        }, 3000)
    }

    render() {
        return (
            <div>
                <h1>Minha conta</h1>

                {this.props.auth.saved &&
                    <Segment color='green'>Salvo com sucesso</Segment>
                }

                {!this.props.auth.saved &&
                    <Form>
                        <Form.Field>
                            <label>Unit</label>
                            <select value={this.state.unit} name='unit' onChange={this.handleChange}>
                                <option value='metric'>Métrico (km)</option>
                                <option value='imperial'>Imperial (mi)</option>
                            </select>
                        </Form.Field>

                        <Form.Field>
                            <label>Timezone</label>
                            <select value={this.state.timezone} name='timezone' onChange={this.handleChange}>
                                {
                                    moment.tz.names().map(tz => {
                                        return <option key={tz} value={tz}>{tz}</option>
                                    })
                                }
                            </select>
                        </Form.Field>

                        <Button onClick={() => this.handleSave()}>Salvar</Button>
                    </Form>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        save: (user) => dispatch(ActionCreators.updateProfileRequest(user)),
        reset: () => dispatch(ActionCreators.updateProfileReset())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyAccount);