import React, { Component } from 'react'
import ActionCreators from '../../redux/actionCreators'
import { connect } from 'react-redux'

import { Button, Segment, Form } from 'semantic-ui-react'

class ChangePass extends Component {
    state = {
        passwd: '',
        passwd2: '',
        error: ''
    }

    componentDidMount() {
        this.props.reset()
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSave = () => {
        if (this.state.passwd !== this.state.passwd2) {

            this.setState({
                error: 'equal'
            })

        } else if (this.state.passwd.length < 6) {

            this.setState({
                error: 'length'
            })

        } else {

            this.setState({ error: '' })

            this.props.save({
                passwd: this.state.passwd,
                id: this.props.auth.user.id
            })

            setTimeout(() => {
                this.props.reset()
            }, 3000)

        }
    }

    render() {
        return (
            <div>
                <h1>Alterar senha</h1>

                {this.state.error === 'equal' &&
                    <Segment color='warning'>As senhas digitadas não conferem.</Segment>
                }

                {this.state.error === 'length' &&
                    <Segment color='warning'>Sua senha é fraca, deve possuir mais que 6 caracteres.</Segment>
                }

                {this.props.auth.saved &&
                    <Segment color='green'>Salvo com sucesso</Segment>
                }

                {this.props.auth.error &&
                    <Segment color='red'>{this.props.auth.errorMessage}</Segment>
                }

                {!this.props.auth.saved &&
                    <Form>
                        <Form.Field>
                            <label>Nova senha</label>
                            <input type='password' value={this.state.passwd} name='passwd' onChange={this.handleChange} />
                        </Form.Field>

                        <Form.Field>
                            <label>Repete nova senha</label>
                            <input type='password' value={this.state.passwd2} name='passwd2' onChange={this.handleChange} />
                        </Form.Field>

                        <Button onClick={() => this.handleSave()}>Salvar</Button>
                    </Form>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        save: (user) => dispatch(ActionCreators.updateProfileRequest(user)),
        reset: () => dispatch(ActionCreators.updateProfileReset())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePass);