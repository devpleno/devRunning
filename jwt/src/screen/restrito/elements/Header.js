import React from 'react'

import { Link } from 'react-router-dom'
import { Menu } from 'semantic-ui-react'

const Header = props => {
    return (
        <Menu>
            <Menu.Item>Corridas Online - Restrito</Menu.Item>

            <Menu.Item as={Link} to='/restrito'>Home</Menu.Item>
            <Menu.Item as={Link} to='/restrito/runs'>Corridas</Menu.Item>
        </Menu>
    )
}


export default Header