import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { connect } from 'react-redux'

import Runs from './Runs'
import Home from './Home'
import ChangePass from './ChangePass'
import MyAccount from './MyAccount'
import CreateRun from './CreateRun'

import Header from './elements/Header'

const Restrito = (props) => {

    if(props.auth.isSigningIn) {
        return <p>Carregando...</p>
    }

    if(!props.auth.isAuth) {
        return <Redirect to='/login' />
    }

    return (
        <div>
            <Header />

            <div>
                <Route component={Home} path={props.match.path + '/'} exact />
                <Route component={Runs} path={props.match.path + '/runs'} />
                <Route component={CreateRun} path={props.match.path + '/create-run'} />
                <Route component={MyAccount} path={props.match.path + '/my-account'} />
                <Route component={ChangePass} path={props.match.path + '/change-pass'} />
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps)(Restrito)