import React, { Component } from 'react';
import ActionCreators from '../../redux/actionCreators'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { Table, Button, Label } from 'semantic-ui-react'

import Duration from '../elements/Duration'
import Distance from '../elements/Distance'
import DateStr from '../elements/DateStr'

class Runs extends Component {

    componentDidMount() {
        this.props.load()
    }

    renderRun = (run, i) => {
        return (
            <Table.Row key={i}>
                {this.props.auth.user.role === 'admin' &&
                    <Table.Cell>
                        <Label>{run.name}</Label>
                    </Table.Cell>
                }

                <Table.Cell>
                    {run.friendly_name}
                </Table.Cell>
                <Table.Cell>
                    <Duration duration={run.duration} />
                </Table.Cell>
                <Table.Cell>
                    <Distance distance={run.distance} metric={this.props.auth.user.unit} />
                </Table.Cell>
                <Table.Cell>
                    <DateStr date={run.created} timezone={this.props.auth.user.timezone} />
                </Table.Cell>
                <Table.Cell>
                    <Button basic color='red' onClick={() => this.props.remove(run.id)}>Excluir</Button>
                </Table.Cell>
            </Table.Row>
        )
    }

    render() {
        return (
            <div>
                <h1>Corridas</h1>
                <Button as={Link} to='/restrito/create-run'>Nova corrida</Button>

                {this.props.runs.isLoading &&
                    <div>Carregando...</div>
                }

                {!this.props.runs.isLoading && this.props.runs.data.length === 0 &&
                    <div>Nenhum registro</div>
                }

                {!this.props.runs.isLoading && this.props.runs.data.length > 0 &&
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                {this.props.auth.user.role === 'admin' &&
                                    <Table.HeaderCell>Usuario</Table.HeaderCell>
                                }

                                <Table.HeaderCell>Friendly Name</Table.HeaderCell>
                                <Table.HeaderCell>Duration</Table.HeaderCell>
                                <Table.HeaderCell>Distance</Table.HeaderCell>
                                <Table.HeaderCell>Created</Table.HeaderCell>
                                <Table.HeaderCell>Ações</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {this.props.runs.data.map((obj, i) => this.renderRun(obj, i))}
                        </Table.Body>
                    </Table>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        runs: state.runs,
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatchLoad: (roleUser) => dispatch(ActionCreators.getRunsRequest(roleUser)),
        remove: (id) => dispatch(ActionCreators.removeRunRequest(id))
    }
}

const mergeProps = (propsFromState, propsFromDispatch) => {
    return {
        ...propsFromState,
        ...propsFromDispatch,
        load: () => propsFromDispatch.dispatchLoad((propsFromState.auth.user.role === 'admin') ? true : false)
    }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Runs)